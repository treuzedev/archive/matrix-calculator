# Matrix Calculator

Inspired by https://hyperskill.org/

A Command Line Program that performs operations on matrices, such as addition, multiplication, transposition, inversion or determinant calculation.

See examples below for more information.


Examples:

```
A calculator for matrices.
Please, use one and only one space to separate numbers.
Please, use one and only one backspace for entering a new line.

-----

1. Add Matrices
2. Multiply a matrix using a scalar
3. Multiply two matrices
4. Transpose a matrix
5. Calculate the determinant of a matrix
6. Inverse a matrix
0. Exit program
Your choice: > 3

-----

Enter size of first matrix: > 2 3
-----
Enter size of second matrix: > 3 2
-----
Enter first matrix values, row by row, column by column:
> 1 2 3
> 4 5 6
-----
Enter second matrix values, row by row, column by column:
> 7 8
> 9 10
> 11 12
-----
First Matrix:
1.0 | 2.0 | 3.0
4.0 | 5.0 | 6.0
-----
Second Matrix:
7.0 | 8.0
9.0 | 10.0
11.0 | 12.0
-----
The multiplication result is:
58.0 | 64.0
139.0 | 154.0

-----

A calculator for matrices.
Please, use one and only one space to separate numbers.
Please, use one and only one backspace for entering a new line.

-----

1. Add Matrices
2. Multiply a matrix using a scalar
3. Multiply two matrices
4. Transpose a matrix
5. Calculate the determinant of a matrix
6. Inverse a matrix
0. Exit program
Your choice: > 6

-----

Enter size of matrix: > 5 5
-----
Enter matrix values, row by row, column by column:
> -1 -2 -3 -4 -5
> 9 8 7 6 5
> 0 0 0 0 0
> 2 5 8 4 5
> 10 20 30 40 50
-----
It's impossible to invert a matrix when the determinant of said matrix is zero.

-----

A calculator for matrices.
Please, use one and only one space to separate numbers.
Please, use one and only one backspace for entering a new line.

-----

1. Add Matrices
2. Multiply a matrix using a scalar
3. Multiply two matrices
4. Transpose a matrix
5. Calculate the determinant of a matrix
6. Inverse a matrix
0. Exit program
Your choice: > 4

-----

1. Main Diagonal
2. Side Diagonal
3. Vertical line
4. Horizontal line
0. Go back
Your choice: > 1
Enter size of matrix: > 3 4
-----
Enter matrix values, row by row, column by column:
> 1 2 3 4
> 5 6 7 8
> 9 10 11 12
-----
Matrix to transpose:
1.0 | 2.0 | 3.0 | 4.0
5.0 | 6.0 | 7.0 | 8.0
9.0 | 10.0 | 11.0 | 12.0
-----
Option:
1
-----
The result is:
1.0 | 5.0 | 9.0
2.0 | 6.0 | 10.0
3.0 | 7.0 | 11.0
4.0 | 8.0 | 12.0

-----

A calculator for matrices.
Please, use one and only one space to separate numbers.
Please, use one and only one backspace for entering a new line.

-----

1. Add Matrices
2. Multiply a matrix using a scalar
3. Multiply two matrices
4. Transpose a matrix
5. Calculate the determinant of a matrix
6. Inverse a matrix
0. Exit program
Your choice: > 4

-----

1. Main Diagonal
2. Side Diagonal
3. Vertical line
4. Horizontal line
0. Go back
Your choice: > 0
A calculator for matrices.
Please, use one and only one space to separate numbers.
Please, use one and only one backspace for entering a new line.

-----

1. Add Matrices
2. Multiply a matrix using a scalar
3. Multiply two matrices
4. Transpose a matrix
5. Calculate the determinant of a matrix
6. Inverse a matrix
0. Exit program
Your choice: > 0

-----

See you next time!

-----
```
