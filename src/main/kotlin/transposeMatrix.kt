// transpose matrix menu
fun transposeMatrixMenu()
{
    // function variables
    val option : Int
    var size : List<String>
    val matrix : ArrayList<ArrayList<Double>>
    val transposedMatrix : ArrayList<ArrayList<Double>>
    var flag : Boolean


    // initialize variables
    flag = true
    size = listOf()


    // asking for user input, and confirm correct input
    println("1. Main Diagonal")
    println("2. Side Diagonal")
    println("3. Vertical line")
    println("4. Horizontal line")
    println("0. Go back")
    print("Your choice: > ")

    option = choiceTranspose()


    // option to go back
    if (option == 0)
    {
        return
    }


    // get user input for matrix size
    while (flag)
    {
        print("Enter size of matrix: > ")
        size = readLine()?.split(" ")!!
        dashes()

        flag = errorCheck(size)
    }


    // get user input to build necessary matrix
    println("Enter matrix values, row by row, column by column:")
    matrix = matrixBuilder(size)
    dashes()


    // print matrix, option, and result
    println("Matrix to transpose:")
    matrixPrinter(matrix)
    dashes()

    println("Option:")
    println(option)
    dashes()

    transposedMatrix = transposeMatrix(option, size, matrix)

    println("The result is:")
    matrixPrinter(transposedMatrix)
    printLine()
}