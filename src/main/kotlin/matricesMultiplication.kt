fun matricesMultiplication()
{
    // function variables
    var matrix1Size : List<String>
    var matrix1 : ArrayList<ArrayList<Double>>
    var matrix2Size : List<String>
    var matrix2 : ArrayList<ArrayList<Double>>
    val matrix3 : ArrayList<ArrayList<Double>>
    var flag : Boolean
    var flag1 : Boolean
    var flag2 : Boolean


    // initialize necessary values
    matrix1Size = listOf()
    matrix2Size = listOf()
    matrix1 = arrayListOf(arrayListOf())
    matrix2 = arrayListOf(arrayListOf())
    flag = true


    // get user input for matrices size
    while (flag)
    {
        print("Enter size of first matrix: > ")
        matrix1Size = readLine()?.split(" ")!!
        dashes()

        print("Enter size of second matrix: > ")
        matrix2Size = readLine()?.split(" ")!!
        dashes()


        // error check for correct input matrices compatible rows vs. columns
        flag1 = errorCheck(matrix1Size, matrix2Size)
        flag2 = multiplyMatrices(matrix1Size, matrix2Size)

        if (!flag1 && !flag2)
        {
            flag = false
        }
    }


    // build matrices
    println("Enter first matrix values, row by row, column by column:")
    matrix1 = matrixBuilder(matrix1Size)
    dashes()

    println("Enter second matrix values, row by row, column by column:")
    matrix2 = matrixBuilder(matrix2Size)
    dashes()


    // print matrices and multiplication result
    println("First Matrix:")
    matrixPrinter(matrix1)
    dashes()

    println("Second Matrix:")
    matrixPrinter(matrix2)
    dashes()

    matrix3 = matrixMultiplication(matrix1, matrix2, matrix1Size, matrix2Size)

    println("The multiplication result is:")
    matrixPrinter(matrix3)
    printLine()
}