// check for choice input error
fun choice() : Int
{
    // function variables
    var input : String
    var flag : Boolean


    // initialize variables
    flag = true
    input = ""


    // check for correct input
    while (flag)
    {
        input = readLine() ?: "none"

        if (input == "none" || input == "" || input.isEmpty() || input.isBlank() || input.toIntOrNull() == null)
        {
            if (input.toIntOrNull() != 0 || input.toIntOrNull() != 1 || input.toIntOrNull() != 2 || input.toIntOrNull() != 3 || input.toIntOrNull() != 4 || input.toIntOrNull() != 5 || input.toIntOrNull() != 6)
            {
                dashes()
                println("Please, enter a valid positive integer between zero and six.")
                dashes()
                print("Your choice: > ")
            }
        }

        else
        {
            flag = false
        }
    }

    return input.toInt()
}


// check for bad input when asking for matrix dimension (addition)
fun errorCheck(size1 : List<String>, size2 : List<String> = listOf()) : Boolean
{
    // two dimensional matrices
    if (size1.size != 2)
    {
        println("Usage for size of first matrix: <number> <number>")
        dashes()
        return true
    }


    // correct input 1
    for (i in size1.indices)
    {
        if (size1[i].toIntOrNull() == null)
        {
            println("Matrix 1 says: Input is not permitted. Only numbers allowed.")
            dashes()
            return true
        }

        else if (size1[i].toInt() <= 0)
        {
            println("Matrix 1 says: Input must be a positive integer.")
            dashes()
            return true
        }
    }


    // correct input 2, if exists
    if (!size2.isNullOrEmpty())
    {
        // correct number of inputs
        if (size2.size != 2)
        {
            println("Usage for size of second matrix: <number> <number>")
            dashes()
            return true
        }


        // correct input
        for (i in size2.indices)
        {
            if (size2[i].toIntOrNull() == null)
            {
                println("Matrix 2 says: Input is not permitted. Only numbers allowed.")
                dashes()
                return true
            }
            else if (size1[i].toInt() <= 0)
            {
                println("Matrix 1 says: Input must be a positive integer.")
                dashes()
                return true
            }
        }
    }

    return false
}


// error check for equal size matrices
fun addEqualMatrices(size1 : List<String>,
                     size2 : List<String>) : Boolean
{
    // input not null
    if (size1.isNullOrEmpty() || size2.isNullOrEmpty() || size1.size == 1 || size2.size == 1)
    {
        println("I'm sorry, you need to input something to make this work)")
        dashes()
        return true
    }


    // correct size
    if (size1[0] != size2[0] || size1[1] != size2[1])
    {
        println("I'm sorry, but both matrices should have an equal number of rows and columns.")
        dashes()
        return true
    }

    return false
}


// error check for rows and columns matrices
fun multiplyMatrices(size1 : List<String>,
                     size2 : List<String>) : Boolean
{
    // not null input
    if (size1.isNullOrEmpty() || size2.isNullOrEmpty() || size1.size == 1 || size2.size == 1)
    {
        println("I'm sorry, you need to input something to make this work)")
        dashes()
        return true
    }


    // correct size
    if (size1[0] != size2[1])
    {
        println("I'm sorry, the number of rows of matrix 1 needs to be exactly equal to the number of columns of matrix 2.")
        dashes()
        return true
    }

    return false
}


// check for choice input error
fun choiceTranspose() : Int
{
    // function variables
    var input : String
    var flag : Boolean


    // initialize variables
    flag = true
    input = ""


    // check for correct input
    while (flag)
    {
        input = readLine() ?: "none"

        if (input == "none" || input == "" || input.isEmpty() || input.isBlank() || input.toIntOrNull() == null )
        {
            if (input.toIntOrNull() != 0 || input.toIntOrNull() != 1 || input.toIntOrNull() != 2 || input.toIntOrNull() != 3 || input.toIntOrNull() != 4)
            {
                dashes()
                println("Please, enter a valid positive integer between zero and four.")
                dashes()
                print("Your choice: > ")
            }
        }

        else
        {
            flag = false
        }
    }

    return input.toInt()
}


// check for bad input when asking for matrix dimension (addition)
fun errorCheckSquare(size1 : List<String>) : Boolean
{
    // two dimensional matrices
    if (size1.size != 2)
    {
        println("Usage for size of first matrix: <number> <number>")
        dashes()
        return true
    }


    // correct input 1
    for (i in size1.indices)
    {
        if (size1[i].toIntOrNull() == null)
        {
            println("Matrix 1 says: Input is not permitted. Only numbers allowed.")
            dashes()
            return true
        }

        else if (size1[i].toInt() <= 0)
        {
            println("Matrix 1 says: Input must be a positive integer.")
            dashes()
            return true
        }
    }


    // check for square matrix
    if (size1[0] != size1[1])
    {
        println("Matrix needs to be square, ie, same number of rows and columns.")
        dashes()
        return true
    }

    return false
}