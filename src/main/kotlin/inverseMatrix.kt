fun inversion()
{
    // function variables
    var matrix1Size : List<String>
    val matrix1 : ArrayList<ArrayList<Double>>
    val matrix2 : ArrayList<ArrayList<Double>>
    val determinant : Double
    var flag : Boolean


    // initialize variables
    flag = true
    matrix1Size = listOf()


    // get user input for matrix size
    while (flag)
    {
        print("Enter size of matrix: > ")
        matrix1Size = readLine()?.split(" ")!!
        dashes()

        flag = errorCheckSquare(matrix1Size)
    }


    // get user input to build necessary matrix
    println("Enter matrix values, row by row, column by column:")
    matrix1 = matrixBuilder(matrix1Size)
    dashes()


    // check for valid determinant
    determinant = matrixDeterminant(matrix1Size, matrix1)

    if (determinant == 0.00)
    {
        println("It's impossible to invert a matrix when the determinant of said matrix is zero")
        printLine()
        return
    }


    // print matrix and inverse
    println("The matrix is:")
    matrixPrinter(matrix1)
    dashes()

    matrix2 = inverseMatrix(matrix1Size, matrix1)
    println("The result is:")
    matrixPrinter(matrix2)
    printLine()
}