fun main()
{
    // function variables
    var choice : Int


    // display main options until exit
    while (true)
    {
        // initial warning
        printWarning()


        // initial menu
        choice = menu()


        // exit program
        when (choice)
        {
            0 -> exit()

            // add two matrices
            1 -> addMatrices()

            // multiply a matrix by a scalar
            2 -> scalarMultiplication()

            // multiply two matrices
            3 -> matricesMultiplication()

            // transpose a matrix
            4 -> transposeMatrixMenu()

            // calculate a determinant of a matrix
            5 -> determinant()

            // invert a matrix
            6 -> inversion()
        }
    }
 }

