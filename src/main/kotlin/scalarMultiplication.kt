fun scalarMultiplication()
{
    // function variables
    var matrix1Size : List<String>
    val matrix1 : ArrayList<ArrayList<Double>>
    val matrix2 : ArrayList<ArrayList<Double>>
    var flag : Boolean
    var scalar : String


    //initialize variables
    flag = true
    scalar = ""
    matrix1Size = listOf()


    // get user input for matrix size
    while (flag)
    {
        print("Enter size of matrix: > ")
        matrix1Size = readLine()?.split(" ")!!
        dashes()

        flag = errorCheck(matrix1Size)
    }


    // get user input to build necessary matrix
    println("Enter matrix values, row by row, column by column:")
    matrix1 = matrixBuilder(matrix1Size)
    dashes()


    // get user input for scalar, while checking for errors
    while(scalar == "")
    {
        scalar = getScalar()
    }


    // print matrix, scalar and result
    dashes()
    println("The matrix is:")
    matrixPrinter(matrix1)
    dashes()
    println("The scalar is:")
    println(scalar)
    dashes()

    matrix2 = matrixScalar(scalar.toDouble(), matrix1)

    println("The scalar multiplication result is:")
    matrixPrinter(matrix2)
    printLine()
}